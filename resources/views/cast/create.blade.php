@extends('adminlte.master');

@section('content')
<div class="ml-3 mt-2" >
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Post</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast" method="GET">
      @csrf
      <div class="card-body">
          
        {{-- <div class="form-group">
          <label for="id">Id</label>
          <input type="text" class="form-control" title="title" id="id" placeholder="Enter id">
        </div> --}}
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" placeholder="Nama" required>
          @error('nama')
          <div class="alert alert-danger">{{$message}}></div>    
          @enderror
          
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" id="umur" placeholder="Umur" required>
            @error('bio')
          <div class="alert alert-danger">{{$message}}></div>    
          @enderror
          </div>
          <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" placeholder="Bio" required>
            @error('bio')
              <div class="alert alert-danger">{{$message}}></div>    
            @enderror
          </div>
            
        </div>

      
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
</div>

@endsection